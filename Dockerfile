## lint image
FROM golangci/golangci-lint:v1.50.1

RUN mkdir -p /build
WORKDIR /build
COPY ./ ./
RUN golangci-lint run

## build image:
FROM golang:1.19.3-bullseye AS builder

RUN apt update && apt install -y make bzip2

RUN mkdir -p /build
WORKDIR /build

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY ./ ./
#RUN make lint
RUN make httpd && mv ./httpd /httpd
RUN go mod vendor
RUN tar -c . | bzip2 > /src.tbz2

## output image:
FROM debian:bullseye-slim AS final
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /httpd /app/httpd
COPY --from=builder /src.tbz2 /usr/local/src/src.tbz2


WORKDIR /app
ENV HOME /app

ENV PORT 8080
ENV DBURL none

EXPOSE 8080

USER nobody:nogroup

ENTRYPOINT ["/app/httpd"]
