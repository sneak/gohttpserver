FN := http

VERSION := $(shell git describe --always --dirty=-dirty)
ARCH := $(shell uname -m)
UNAME_S := $(shell uname -s)
GOLDFLAGS += -X main.Version=$(VERSION)
GOLDFLAGS += -X main.Buildarch=$(ARCH)
GOFLAGS := -ldflags "$(GOLDFLAGS)"

default: clean debug

commit: fmt lint
	git commit -a

# get golangci-lint with:
# go get github.com/golangci/golangci-lint/cmd/golangci-lint@v1.31.0
# get gofumports with:
# go get mvdan.cc/gofumpt/gofumports
fmt:
	gofumpt -l -w .
	golangci-lint run --fix

lint:
	golangci-lint run
	sh -c 'test -z "$$(gofmt -l .)"'

debug: ./$(FN)d
	DEBUG=1 GOTRACEBACK=all ./$(FN)d

debugger:
	cd cmd/$(FN)d && dlv debug main.go

run: ./$(FN)d
	./$(FN)d

clean:
	-rm -f ./$(FN)d debug.log

docker:
	docker build --progress plain .

./$(FN)d: cmd/$(FN)d/main.go internal/*/*.go templates/* static/*
	cd ./cmd/$(FN)d && \
		go build -o ../../$(FN)d $(GOFLAGS) .

tools:
	go get -v github.com/golangci/golangci-lint/cmd/golangci-lint@v1.31.0
	go get -v mvdan.cc/gofumpt/gofumports
