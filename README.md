# gohttpserver

[![Build Status](https://drone.datavi.be/api/badges/sneak/gohttpserver/status.svg)](https://drone.datavi.be/sneak/gohttpserver)

This is my boilerplate for a go HTTP server, designed to be a
starting point template for new projects, with most things conveniently
stubbed out and ready for simple and fast customization, with sane defaults.

Many ideas are taken from Mat Ryer's talk titled "How I Write HTTP Web Services
after Eight Years" at GopherCon 2019, seen here:

https://www.youtube.com/watch?v=rWBSMsLG8po

# Contributing

Contributions are welcome!  Please send me an email if you'd like an account
on this server to submit PRs.

Alternately, even just feedback is great:
[sneak@sneak.berlin](mailto:sneak@sneak.berlin)

# Features

* Basic logging middleware
* Stub Authentication middleware
* Helper functions for encoding/decoding json
* Healthcheck route
* Prometheus metrics endpoint
* No global state of our own
    * some deps have some, such as the metrics collector and Sentry

# Design Decisions

* no TLS in this code
    * do it somewhere else, like on a sidecar or reverse proxy
    * this might have to change to support http > 1 later
* logging: [rs/zerolog](https://github.com/rs/zerolog)
* configuration: [spf13/viper](https://github.com/spf13/viper)
    * used as a wrapper around env vars, because of typed getters
* router is Chi: [go-chi/chi](https://github.com/go-chi/chi)
* Prometheus-style metrics via [slok/go-http-metrics](https://github.com/slok/go-http-metrics)
* code formatted with [mvdan.cc/gofumpt](https://mvdan.cc/gofumpt)
* code style checked with [golangci/golangci-lint](https://github.com/golangci/golangci-lint)

# Pending Design Decisions

* database: TBD (thinking about [go-gorm/gorm](https://github.com/go-gorm/gorm))
* templating: TBD (suggestions welcome)

# TODO

* Basic HTML Templates
* Database Boilerplate
* Sessions Middleware
* sync.Once example for re-compiling templates
* Bundling static assets into binary

# Known Bugs (more TODO)

* Chi recovery middleware logs non-json when in non-tty stdout mode,
  breaking validity of stdout as a json stream

# Author

* [sneak@sneak.berlin](mailto:sneak@sneak.berlin)
* https://sneak.berlin
* [@sneak@sneak.berlin](https://s.sneak.berlin/@sneak)

# License

WTFPL (aka public domain):

```
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
```

