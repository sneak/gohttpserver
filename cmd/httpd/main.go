package main

import (
	"git.eeqj.de/sneak/gohttpserver/internal/config"
	"git.eeqj.de/sneak/gohttpserver/internal/database"
	"git.eeqj.de/sneak/gohttpserver/internal/globals"
	"git.eeqj.de/sneak/gohttpserver/internal/handlers"
	"git.eeqj.de/sneak/gohttpserver/internal/healthcheck"
	"git.eeqj.de/sneak/gohttpserver/internal/logger"
	"git.eeqj.de/sneak/gohttpserver/internal/middleware"
	"git.eeqj.de/sneak/gohttpserver/internal/server"
	"go.uber.org/fx"
)

var (
	Appname   string = "CHANGEME"
	Version   string
	Buildarch string
)

func main() {
	globals.Appname = Appname
	globals.Version = Version
	globals.Buildarch = Buildarch

	fx.New(
		fx.Provide(
			config.New,
			database.New,
			globals.New,
			handlers.New,
			logger.New,
			server.New,
			middleware.New,
			healthcheck.New,
		),
		fx.Invoke(func(*server.Server) {}),
	).Run()
	// os.Exit(server.Run(Appname, Version, Buildarch))
}
