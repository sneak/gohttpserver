package database

import (
	"context"

	"git.eeqj.de/sneak/gohttpserver/internal/config"
	"git.eeqj.de/sneak/gohttpserver/internal/logger"
	"github.com/rs/zerolog"
	"go.uber.org/fx"

	// spooky action at a distance!
	// this populates the environment
	// from a ./.env file automatically
	// for development configuration.
	// .env contents should be things like
	// `DBURL=postgres://user:pass@.../`
	// (without the backticks, of course)
	_ "github.com/joho/godotenv/autoload"
)

type DatabaseParams struct {
	fx.In
	Logger *logger.Logger
	Config *config.Config
}

type Database struct {
	URL    string
	log    *zerolog.Logger
	params *DatabaseParams
}

func New(lc fx.Lifecycle, params DatabaseParams) (*Database, error) {
	s := new(Database)
	s.params = &params
	s.log = params.Logger.Get()

	s.log.Info().Msg("Database instantiated")

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			s.log.Info().Msg("Database OnStart Hook")
			// FIXME connect to db
			return nil
		},
		OnStop: func(ctx context.Context) error {
			// FIXME disconnect from db
			return nil
		},
	})
	return s, nil
}
