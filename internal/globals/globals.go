package globals

import (
	"go.uber.org/fx"
)

// these get populated from main() and copied into the Globals object.
var (
	Appname   string
	Version   string
	Buildarch string
)

type Globals struct {
	Appname   string
	Version   string
	Buildarch string
}

func New(lc fx.Lifecycle) (*Globals, error) {
	n := &Globals{
		Appname:   Appname,
		Buildarch: Buildarch,
		Version:   Version,
	}
	return n, nil
}
