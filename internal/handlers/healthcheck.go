package handlers

import (
	"net/http"
)

func (s *Handlers) HandleHealthCheck() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		resp := s.hc.Healthcheck()
		s.respondJSON(w, req, resp, 200)
	}
}
