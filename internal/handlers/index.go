package handlers

import (
	"net/http"

	"git.eeqj.de/sneak/gohttpserver/templates"
)

func (s *Handlers) HandleIndex() http.HandlerFunc {
	t := templates.GetParsed()

	return func(w http.ResponseWriter, r *http.Request) {
		err := t.ExecuteTemplate(w, "index.html", nil)
		if err != nil {
			s.log.Error().Err(err).Msg("")
			http.Error(w, http.StatusText(500), 500)
		}
	}
}
