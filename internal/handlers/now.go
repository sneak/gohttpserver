package handlers

import (
	"net/http"
	"time"
)

func (s *Handlers) HandleNow() http.HandlerFunc {
	type response struct {
		Now time.Time `json:"now"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		s.respondJSON(w, r, &response{Now: time.Now()}, 200)
	}
}
