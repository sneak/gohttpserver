package handlers

import (
	"net/http"
)

// CHANGEME you probably want to remove this,
// this is just a handler/route that throws a panic to test
// sentry events.
func (s *Handlers) HandlePanic() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		panic("y tho")
	}
}
