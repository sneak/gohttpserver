package server

import (
	"fmt"
	"net/http"
	"time"
)

func (s *Server) serveUntilShutdown() {
	listenAddr := fmt.Sprintf(":%d", s.params.Config.Port)
	s.httpServer = &http.Server{
		Addr:           listenAddr,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
		Handler:        s,
	}

	// add routes
	// this does any necessary setup in each handler
	s.SetupRoutes()

	s.log.Info().Str("listenaddr", listenAddr).Msg("http begin listen")
	if err := s.httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		s.log.Error().Msgf("listen:%+s\n", err)
		if s.cancelFunc != nil {
			s.cancelFunc()
		}
	}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
