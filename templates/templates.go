package templates

import (
	"embed"
	"text/template"
)

//go:embed *.html
var TemplatesRaw embed.FS
var TemplatesParsed *template.Template

func GetParsed() *template.Template {
	if TemplatesParsed == nil {
		TemplatesParsed = template.Must(template.ParseFS(TemplatesRaw, "*"))
	}
	return TemplatesParsed
}

/*
func MustString(filename string) string {
	bytes, error := Templates.ReadFile(filename)
	if error != nil {
		panic(error)
	}
	var out strings.Builder
	out.Write(bytes)
	return out.String()
}
*/
